#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>

#define BUFFER_LENGTH 1024              ///< The buffer length (crude but fine)
static char receive[BUFFER_LENGTH];     ///< The receive buffer from the LKM

int main(){
   int ret, fd;
   char stringToSend[BUFFER_LENGTH];
   printf("Loading device driver...\n");
   fd = open("/dev/ebbchar", O_RDWR);             
   if (fd < 0){
      perror("Failed to open the device...");
      return errno;
   }
   printf("What to send to the kernel module:\n");
   scanf("%[^\n]%*c", stringToSend);
   printf("Writing [%s] to the device.\n", stringToSend);
   ret = write(fd, stringToSend, strlen(stringToSend));
   if (ret < 0){
      perror("Failed to write the message to the device.");
      return errno;
   }

   printf("Press any key to read from device...\n");
   getchar();

   printf("Reading from the device...\n");
   ret = read(fd, receive, BUFFER_LENGTH);     
   if (ret < 0){
      perror("Failed to read the message from the device.");
      return errno;
   }
   printf("The received message is: [%s]\n", receive);
   printf("End of the test program\n");
   return 0;
}
