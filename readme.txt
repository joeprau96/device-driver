How To Use:
  run "make"         this sets everything up
  run "sudo insmod ebbchar.ko"  this adds the module to the system
  run "sudo ./test"  this test read and write on the module

How To Stop:
  run "sudo rmmod ebbchar"  this removes the module from the system
  run "make clean"          this makes the directory clean again


Notes: 
  On line 17 the defined MAX_SIZE can be changed to any Positive Non-Zero Integer and will not lose written data.
